package com.ahsj.crawler;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.scheduler.BloomFilterDuplicateRemover;
import us.codecraft.webmagic.scheduler.QueueScheduler;
import us.codecraft.webmagic.scheduler.Scheduler;

/**
 * @author Eastern unbeaten
 * @version 1.0
 * @date 2019/6/9 0:28
 * @mail chenshiyun2011@163.com
 */
public class JdprocessorMainTest implements PageProcessor {

    static String pathUrl = "https://search.jd.com/Search?keyword=%E6%89%8B%E6%9C%BA&enc=utf-8&wq=%E6%89%8B%E6%9C%BA&pvid=6a05de8c3c474b2f9a4c8cb487829623";

    @Override
    public void process(Page page) {
        String url = page.getUrl().toString();
        System.out.println(url);
        String s1 = page.getHtml().css("div#J_goodsList > ul > li").toString();
        if (s1==null){
            String s = page.getHtml().toString();
            System.out.println(s);
            page.addTargetRequest(url);
        }else{
        }
    }

    private Site site = Site
            .me()
            .setCharset("UTF8")
            //超时时间
            .setTimeOut(3000)
            //重试时间
            .setRetryTimes(3000)
            //重试次数
            .setSleepTime(3);

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {
        //添加地址
        Spider spider = Spider.create(new JdprocessorMainTest()).addUrl(pathUrl);
        //添加过滤器
        spider.setScheduler(new QueueScheduler().setDuplicateRemover(new BloomFilterDuplicateRemover(100000)));
        //设置线程
        spider.thread(10);
        //获取过滤器名称
        Scheduler scheduler = spider.getScheduler();
        //运行
        spider.run();
    }
}
