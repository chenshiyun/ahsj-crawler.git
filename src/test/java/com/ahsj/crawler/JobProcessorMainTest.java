package com.ahsj.crawler;

import com.ahsj.crawler.pojo.JobInfo;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.scheduler.BloomFilterDuplicateRemover;
import us.codecraft.webmagic.scheduler.QueueScheduler;
import us.codecraft.webmagic.scheduler.Scheduler;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Eastern unbeaten
 * @email chenshiyun2011@163.com
 * @data 2019/5/27
 */
public class JobProcessorMainTest implements PageProcessor {
    static String path = "https://search.51job.com/list/030200,000000,0000,00,9,99,java,2,1.html?lang=c&stype=&postchannel=0000&workyear=99&cotype=99&degreefrom=99&jobterm=99&companysize=99&providesalary=99&lonlat=0%2C0&radius=-1&ord_field=0&confirmdate=9&fromType=&dibiaoid=0&address=&line=&specialarea=00&from=&welfare=";

    @Override
    public void process(Page page) {
        List<Selectable> selectables = page.getHtml().css("div#resultList div.el").nodes();
        //判断集合是否是空
        if (selectables.size() == 0) {
            this.saveJobInfo(page);
        } else {
            //不空就是列表
            //如果空表示这个是招聘页
            for (Selectable selectable : selectables) {
                String jobInfoUrl = selectable.links().toString();
                System.out.println(jobInfoUrl);
                //详情放到队列
                page.addTargetRequest(jobInfoUrl);
            }
            //获取下一页
            String bkUrl = page.getHtml().css("div.p_in li.bk").nodes().get(1).links().toString();
            //把url放队列
            //page.addTargetRequest(bkUrl);
        }
    }

    private void saveJobInfo(Page page) {
        Html html = page.getHtml();
        JobInfo obj = new JobInfo();
        String s = html.css("div.tHjob").toString();
        //获取到公司名称
        String companyName = html.css("div.cn p.cname a", "text").toString();
        obj.setCompanyName(companyName);
        String companyAddrHtml = html.css("div.bmsg").nodes().get(1).toString();
        //获取到公司地址
        String companyAddr = Jsoup.parse(companyAddrHtml).text();
        obj.setCompanyAddr(companyAddr);
        String companyInfoHtml = html.css("div.tmsg").toString();
        //公司详情
        String companyInfo = Jsoup.parse(companyInfoHtml).text();
        obj.setCompanyInfo(companyInfo);
        //岗位名称
        String jobName = html.css("div.cn h1", "text").toString();
        obj.setJobName(jobName);
        //工作地址
        String jobAddr = html.css("div.bmsg").nodes().get(1).css("p", "text").toString();
        obj.setJobAddr(jobAddr);
        //岗位详情
        String jobInfoHtml = html.css("div.job_msg").toString();
        //岗位详情
        String jobInfo = Jsoup.parse(jobInfoHtml).text();
        obj.setJobInfo(jobInfo);
        //爬取地址
        String url = page.getUrl().toString();
        obj.setUrl(url);
        String pay = html.css("div.cn strong", "text").toString();
        String[] psya = payUtils(pay);
        String min = psya[0];
        obj.setSalaryMin(min);
        String max = psya[1];
        obj.setSalaryMax(max);
        String timeHtml = html.css("p.ltype", "title").toString();
        String time = sbTime(timeHtml);
        obj.setTime(time);
        //保存对象就行了
        page.putField("jobinfo", obj);
        // xxx.add(obj)
    }

    /**
     *  |  3-4年经验  |  大专  |  招若干人  |  06-01发布
     * 广州|3-4年经验|大专|招若干人|06-01发布
     *
     * @param timeHtml 分割
     * @return
     */
    private static String sbTime(String timeHtml) {
        if (StringUtils.isNoneBlank(timeHtml)) {
            //去除英文空格
            timeHtml = timeHtml.replace(" ", "");
            //去除中文空格
            timeHtml = timeHtml.replace(" ", "");
            //        我们需要切割的格式 |  3-4年经验  |  大专  |  招若干人  |  06-01发布
            String[] splits = timeHtml.split("\\|");
            if (StringUtils.isNoneBlank(splits)) {
                String regEx = "发布.*";
                for (String split : splits) {
                    // 正则出发布时间,不一定准
                    if (StringUtils.isNoneBlank(split)) {
                        Pattern pattern = Pattern.compile(regEx);
                        Matcher matcher = pattern.matcher(split);
                        boolean b = matcher.find();
                        if (b) {
                            return split;
                        }
                    }
                }

            }
        }
        return "";
    }

    /**
     * 固定格式,否则就gg   xx-xT/??
     * 解析结果  xx+T   xT
     *
     * @param pay [xx+T,xT]   0.8-1万/月 = [0.8万,1万]    6-8千/月  = [6千,8千]
     * @return
     */
    private static String[] payUtils(String pay) {
        String[] strings = new String[]{"", ""};
        if (StringUtils.isNoneBlank(pay)) {
            String[] splits = pay.split("/");
            if (ArrayUtils.isNotEmpty(splits)) {
                String[] split = splits[0].split("-");
                if (ArrayUtils.isNotEmpty(split) && split.length == 2) {
                    //取出第一个收入
                    String s1 = split[0];
                    //提出第二个收入
                    String s2 = split[1];
                    if (StringUtils.isNoneBlank(s2)) {
                        //取出第二个的单位放给第一个
                        Character c = s2.charAt(s2.length() - 1);
                        //单位放给第一个
                        s1 = s1.concat(c.toString());
                    }
                    strings[0] = s1;
                    strings[1] = s2;
                }
            }


        }

        return strings;
    }

    private Site site = Site
            .me()
            .setCharset("gbk")
            //超时时间
            .setTimeOut(3000)
            //重试时间
            .setRetryTimes(3000)
            //重试次数
            .setSleepTime(3);

    @Override
    public Site getSite() {
        return site;
    }


    public static void main(String[] args) {
        Spider spider = Spider.create(new JobProcessorMainTest()).addUrl(path);
        spider.setScheduler(new QueueScheduler().setDuplicateRemover(new BloomFilterDuplicateRemover(100000)));
        spider.thread(10);
        Scheduler scheduler = spider.getScheduler();
        spider.run();


    }

}
