package com.ahsj.crawler.service.impl;

import com.ahsj.crawler.dao.JobInfoDao;
import com.ahsj.crawler.pojo.JobInfo;
import com.ahsj.crawler.service.JobInfoService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author Eastern unbeaten
 * @email chenshiyun2011@163.com
 * @data 2019/5/15
 */
@Service
@Log4j2
public class JobInfoServiceImpl implements JobInfoService {

    @Autowired
    private JobInfoDao jobInfoDao;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(JobInfo jobInfo) {
        //根据url和发布时间查询数据
        JobInfo parameter = new JobInfo();
        parameter.setUrl(jobInfo.getUrl());
        parameter.setTime(jobInfo.getTime());
        List<JobInfo> list = this.findAll(parameter);
        if (list.size() == 0) {
            //更新或者插入,如果不存在的话
            jobInfoDao.saveAndFlush(jobInfo);
        } else {
            log.debug("url:"+jobInfo.getUrl()+",发布时间:"+jobInfo.getTime()+"已经存在");
        }
    }

    @Override
    public List<JobInfo> findAll(JobInfo jobInfo) {
        //配置查询条件
        Example<JobInfo> example = Example.of(jobInfo);
        List<JobInfo> list = jobInfoDao.findAll(example);
        return list;
    }

    @Override
    public List<JobInfo> list() {
        List<JobInfo> list = jobInfoDao.findAll();
        return list;
    }
}
