package com.ahsj.crawler.service;

import com.ahsj.crawler.pojo.JobInfo;

import java.util.List;

/**
 * @author Eastern unbeaten
 * @email chenshiyun2011@163.com
 * @data 2019/5/15
 */
public interface JobInfoService {

    void save(JobInfo jobInfo);

    List<JobInfo> findAll(JobInfo jobInfo);

    List<JobInfo> list();
}
