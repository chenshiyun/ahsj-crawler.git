package com.ahsj.crawler.task;

import com.ahsj.crawler.pojo.JobInfo;
import com.ahsj.crawler.util.StringSbUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections.CollectionUtils;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.scheduler.BloomFilterDuplicateRemover;
import us.codecraft.webmagic.scheduler.QueueScheduler;
import us.codecraft.webmagic.scheduler.Scheduler;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import java.util.List;

/**
 * @author Eastern unbeaten
 * @email chenshiyun2011@163.com
 * @data 2019/5/27
 */
@Component
@Log4j2
public class JobProcessorMain implements PageProcessor {
    static String path = "https://search.51job.com/list/030200,000000,0000,00,9,99,java,2,1.html?lang=c&stype=&postchannel=0000&workyear=99&cotype=99&degreefrom=99&jobterm=99&companysize=99&providesalary=99&lonlat=0%2C0&radius=-1&ord_field=0&confirmdate=9&fromType=&dibiaoid=0&address=&line=&specialarea=00&from=&welfare=";

    @Autowired
    SpringDataPipeLine springDataPipeLine;

    @Override
    public void process(Page page) {
        //取出招聘列表
        List<Selectable> selectables = page.getHtml().css("div#resultList div.el").nodes();
        //判断集合是否是空
        if (CollectionUtils.isEmpty(selectables)) {
            //保存详情
            this.saveJobInfo(page);
        } else {
            //处理招聘列表信息.按照元素分割好
            if (CollectionUtils.isNotEmpty(selectables)) {
                //迭代出没一行招聘信息
                for (Selectable selectable : selectables) {
                    //取出详情的url
                    String jobInfoUrl = selectable.links().toString();
                    //详情放到队列
                    page.addTargetRequest(jobInfoUrl);
                }
            }
            //获取下一页
            String bkUrl = page.getHtml().css("div.p_in li.bk").nodes().get(1).links().toString();
            log.debug("下一页的地址是:" + bkUrl);
            //把url放队列....目前是调试,把下一页爬取关掉了
            //page.addTargetRequest(bkUrl);
        }
    }

    /**
     * 处理招聘信息详情
     *
     * @param page
     */
    private void saveJobInfo(Page page) {
        log.debug("当前需要爬取的url" + page.getUrl().toString());
        Html html = page.getHtml();
        JobInfo obj = new JobInfo();
        //获取到公司名称
        String companyName = html.css("div.cn p.cname a", "text").toString();
        obj.setCompanyName(companyName);
        String companyAddrHtml = html.css("div.bmsg").nodes().get(1).toString();
        //获取到公司地址
        String companyAddr = Jsoup.parse(companyAddrHtml).text();
        obj.setCompanyAddr(companyAddr);
        String companyInfoHtml = html.css("div.tmsg").toString();
        //公司详情
        String companyInfo = Jsoup.parse(companyInfoHtml).text();
        obj.setCompanyInfo(companyInfo);
        //岗位名称
        String jobName = html.css("div.cn h1", "text").toString();
        obj.setJobName(jobName);
        //工作地址
        String jobAddr = html.css("div.bmsg").nodes().get(1).css("p", "text").toString();
        obj.setJobAddr(jobAddr);
        //岗位详情
        String jobInfoHtml = html.css("div.job_msg").toString();
        //岗位详情
        String jobInfo = Jsoup.parse(jobInfoHtml).text();
        obj.setJobInfo(jobInfo);
        //爬取地址
        String url = page.getUrl().toString();
        obj.setUrl(url);
        String pay = html.css("div.cn strong", "text").toString();
        String[] psya = StringSbUtils.payUtils(pay);
        String min = psya[0];
        obj.setSalaryMin(min);
        String max = psya[1];
        obj.setSalaryMax(max);
        String timeHtml = html.css("p.ltype", "title").toString();
        String time = StringSbUtils.sbTime(timeHtml);
        log.debug("需要切割的发布时间:" + timeHtml + ",切割后的结果:" + time);
        obj.setTime(time);
        log.debug("完整数据爬取结果:" + obj.toString());
        page.putField("jobinfo", obj);
    }




    private Site site = Site
            .me()
            .setCharset("gbk")
            //超时时间
            .setTimeOut(3000)
            //重试时间
            .setRetryTimes(3000)
            //重试次数
            .setSleepTime(3);

    @Override
    public Site getSite() {
        return site;
    }


    /**
     * 调试使用....所以时间调的比较大
     */
    @Scheduled(initialDelay = 3000, fixedDelay = 1000 * 1000)
    public void process() {
        Spider spider = Spider.create(new JobProcessorMain()).addUrl(path);
        //使用布隆过滤器,simhash过滤器已经解除引用
        spider.setScheduler(new QueueScheduler().setDuplicateRemover(new BloomFilterDuplicateRemover(100000)));
        spider.thread(10);
        spider.addPipeline(springDataPipeLine);
        Scheduler scheduler = spider.getScheduler();

        spider.run();
    }

}
