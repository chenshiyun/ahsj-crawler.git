package com.ahsj.crawler.task;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Log4j2
public class Init {
    protected final Log logger = LogFactory.getLog(getClass());

    @PostConstruct
    public void init( ) {
        logger.info("==================info 日志级别 ============");
        logger.debug("==================debug 日志级别 ============");
        logger.warn("==================warn 日志级别 ============");
        logger.error("==================error 日志级别 ============");
        System.out.println("init==============================");
    }
}
