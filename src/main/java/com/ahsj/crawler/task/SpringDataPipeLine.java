package com.ahsj.crawler.task;

import com.ahsj.crawler.pojo.JobInfo;
import com.ahsj.crawler.service.JobInfoService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.Objects;
@Log
@Component
public class SpringDataPipeLine implements Pipeline {

    @Autowired
    private JobInfoService jobInfoService;
    @Override
    public void process(ResultItems resultItems, Task task) {
        JobInfo jobinfo = resultItems.get("jobinfo");
        //判断数据书否空
        if(Objects.nonNull(jobinfo)){
            //存储到数据库
            jobInfoService.save(jobinfo);
        }
    }
}
