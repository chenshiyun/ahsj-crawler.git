package com.ahsj.crawler.task;

import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.downloader.HttpClientDownloader;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.proxy.Proxy;
import us.codecraft.webmagic.proxy.SimpleProxyProvider;

/**
 * 测试代理服务
 */
@Component
public class ProxyTest implements PageProcessor {


    //    @Scheduled(fixedDelay = 1000)
    public void process() {
        //创建下载器 httpClientDownLoader
        HttpClientDownloader httpClientDownloader = new HttpClientDownloader();
        //给下载器设置代理服务信息
        httpClientDownloader.setProxyProvider(SimpleProxyProvider.from(new Proxy("117.191.11.102", 80)));
        Spider.create(new ProxyTest()).addUrl("http://ip.tool.chinaz.com/")
                //设置代理
                .setDownloader(httpClientDownloader)
                .run();

    }

    @Override
    public void process(Page page) {
        System.out.println(page.getHtml().toString());

    }

    private Site site = Site.me();

    @Override
    public Site getSite() {
        return site;
    }


}
