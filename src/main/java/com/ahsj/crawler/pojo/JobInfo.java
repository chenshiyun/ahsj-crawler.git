package com.ahsj.crawler.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;


/**
 * @author Eastern unbeaten
 * @email chenshiyun2011@163.com
 * @data 2019/5/15
 */
@Entity
@Table(name = "job_info")
@Setter
@Getter
@ToString
public class JobInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private Long id;
    private String url;
    private String time;
    private String companyName;
    private String companyAddr;
    private String companyInfo;
    private String jobName;
    private String jobAddr;
    private String jobInfo;
    private String salaryMin;
    private String salaryMax;


    public JobInfo() {
    }
}
