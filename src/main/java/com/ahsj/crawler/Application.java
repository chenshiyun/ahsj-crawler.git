package com.ahsj.crawler;

import com.ahsj.crawler.service.JobInfoService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.logging.Level;

@SpringBootApplication
@RestController
//定时开启
@EnableScheduling
@Log
public class Application {

	@Autowired
	private JobInfoService itemService;

	public static void main(String[] args) throws IOException {
		SpringApplication.run(Application.class, args);
	}

	@RequestMapping("/")
	public String hello() {
		if(log.isLoggable(Level.INFO)){
			log.info("test");
		}
		return "hello";
	}

	@RequestMapping("/list")
	public Object list() {
		return itemService.list();
	}
}
