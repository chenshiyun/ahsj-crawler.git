package com.ahsj.crawler.util;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Eastern unbeaten
 * @version 1.0
 * @date 2019/6/4 22:41
 * @mail chenshiyun2011@163.com
 */
public interface StringSbUtils {

    /**
     *  |  3-4年经验  |  大专  |  招若干人  |  06-01发布
     * 广州|3-4年经验|大专|招若干人|06-01发布
     *
     * @param timeHtml 分割
     * @return
     */
    static String sbTime(String timeHtml) {
        if (StringUtils.isNoneBlank(timeHtml)) {
            //去除英文空格
            timeHtml = timeHtml.replace(" ", "");
            //去除中文空格
            timeHtml = timeHtml.replace(" ", "");
            String[] splits = timeHtml.split("\\|");
            if (StringUtils.isNoneBlank(splits)) {
                String regEx = "发布.*";
                for (String split : splits) {
                    // 正则出发布时间,不一定准
                    if (StringUtils.isNoneBlank(split)) {
                        Pattern pattern = Pattern.compile(regEx);
                        Matcher matcher = pattern.matcher(split);
                        boolean b = matcher.find();
                        if (b) {
                            return split;
                        }
                    }
                }

            }
        }
        return "";
    }

    /**
     * 固定格式,否则就gg   xx-xT/??
     * 解析结果  xx+T   xT
     *
     * @param pay [xx+T,xT]   0.8-1万/月 = [0.8万,1万]    6-8千/月  = [6千,8千]
     * @return
     */
     static String[] payUtils(String pay) {
        String[] strings = new String[]{"", ""};
        if (StringUtils.isNoneBlank(pay)) {
            String[] splits = pay.split("/");
            if (ArrayUtils.isNotEmpty(splits)) {
                String[] split = splits[0].split("-");
                if (ArrayUtils.isNotEmpty(split) && split.length == 2) {
                    //取出第一个收入
                    String s1 = split[0];
                    //提出第二个收入
                    String s2 = split[1];
                    if (StringUtils.isNoneBlank(s2)) {
                        //取出第二个的单位放给第一个
                        Character c = s2.charAt(s2.length() - 1);
                        //单位放给第一个
                        s1 = s1.concat(c.toString());
                    }
                    strings[0] = s1;
                    strings[1] = s2;
                }
            }
        }
        return strings;
    }


}
