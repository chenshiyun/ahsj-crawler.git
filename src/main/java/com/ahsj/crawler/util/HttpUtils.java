package com.ahsj.crawler.util;

import lombok.extern.log4j.Log4j2;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;
import java.util.UUID;

/**
 * @author Eastern unbeaten
 * @email chenshiyun2011@163.com
 * @data 2019/5/16
 */
@Component
@Log4j2
public class HttpUtils {

    static String imgPath = "C:\\Users\\Administrator\\Desktop\\imgPath";

    private PoolingHttpClientConnectionManager cm;

    public HttpUtils() {
        cm = new PoolingHttpClientConnectionManager();
        //设置最大连接
        cm.setMaxTotal(100);
        //设置每个主机最大连接数
        cm.setDefaultMaxPerRoute(10);
    }

    public String doGetHtml(String url) {
        //获取httpClient对象
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();
        //创建get请求对象,设置url地址
        HttpGet httpGet = new HttpGet(url);
        log.info("需要请求的url:" + url);
        //设置请求信息
        httpGet.setConfig(this.getConfig());

        //使用httpClient发送请求,获取响应
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(httpGet);
            //解析响应,返回结果
            if (Objects.nonNull(response) && response.getStatusLine().getStatusCode() == 200) {
                if (Objects.nonNull(response.getEntity())) {
                    String content = EntityUtils.toString(response.getEntity(), "utf8");
                    return content;
                }
            }
        } catch (IOException e) {
            log.error("httpClient网络请求失败");
            e.printStackTrace();
        } finally {
            closeResponse(response);
        }
        return "";
    }

    public String doGetImage(String url) {
        //获取httpClient对象
        CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();
        //创建get请求对象,设置url地址
        HttpGet httpGet = new HttpGet(url);

        //设置请求信息
        httpGet.setConfig(getConfig());

        //使用httpClient发送请求,获取响应
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(httpGet);
            //解析响应,返回结果
            if (Objects.nonNull(response) && response.getStatusLine().getStatusCode() == 200) {
                if (Objects.nonNull(response.getEntity())) {
                    //下载图片
                    //获取图片后缀
                    String extName = url.substring(url.lastIndexOf("."));
                    //创建图片重命名
                    String picName = UUID.randomUUID().toString() + extName;
                    //下载图片
                    String filepath = imgPath +"\\"+ picName;
                   log.info("图片上传地址:"+filepath);
                    OutputStream fileOutputStream = new FileOutputStream(new File(filepath));
                    response.getEntity().writeTo(fileOutputStream);
                    //返回图片名称
                    return picName;
                }
            }
        } catch (IOException e) {
            log.error("httpClient网络请求失败");
            e.printStackTrace();
        } finally {
            closeResponse(response);
        }
        return "";
    }

    private static void closeResponse(CloseableHttpResponse response) {
        if (Objects.nonNull(response)) {
            try {
                response.close();
            } catch (IOException e) {
                log.error("httpClient网络请求的资源关闭失败");
                e.printStackTrace();
            }
        }
    }

    public RequestConfig getConfig() {
        RequestConfig config = RequestConfig.custom()
                .setConnectTimeout(1000) //创建链接时长
                .setConnectionRequestTimeout(500) //获取的最长时间
                .setSocketTimeout(10000).build(); //数据传输的最长时间
        return config;
    }
}

