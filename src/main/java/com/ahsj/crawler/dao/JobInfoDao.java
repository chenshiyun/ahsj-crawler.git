package com.ahsj.crawler.dao;

import com.ahsj.crawler.pojo.JobInfo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Eastern unbeaten
 * @email chenshiyun2011@163.com
 * @data 2019/5/15
 */
public interface JobInfoDao extends JpaRepository<JobInfo,Long>{
}
