DROP TABLE IF EXISTS `job_info`;
CREATE TABLE `job_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'key',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '详情地址',
  `time` varchar(100) DEFAULT '' COMMENT '发布时间',
  `company_name` varchar(200) DEFAULT '' COMMENT '岗位名称',
  `company_addr` varchar(200) DEFAULT '' COMMENT '岗位地址',
  `company_info` text COMMENT '详情',
  `job_name` varchar(200) DEFAULT '' COMMENT '工作名字',
  `job_addr` varchar(200) DEFAULT '' COMMENT '工作地址',
  `job_info` text COMMENT '工作详情',
  `salary_min` varchar(100) DEFAULT '' COMMENT '薪资范围小',
  `salary_max` varchar(100) DEFAULT '' COMMENT '薪资范围大',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;